from django.db import models
from django.utils import timezone

class Post(models.Model): # Objecte que defineix el nostre model 
    # Definim les propietats:
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE) # link que relaciona amb altre model
    title = models.CharField(max_length=200) # texte limitat per (x) caracters
    text = models.TextField() # texte sense limits
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self): # metode 
        self.published_date = timezone.now() 
        self.save()

    def __str__(self):
        return self.title

